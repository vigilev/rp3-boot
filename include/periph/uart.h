#ifndef PERIPH_UART_H
#define PERIPH_UART_H
#include <periph/base.h>

/* PL011 UART registers */
#define UART0_DR        ((volatile unsigned int*)(PBASE+0x00201000))
#define UART0_FR        ((volatile unsigned int*)(PBASE+0x00201018))
#define UART0_IBRD      ((volatile unsigned int*)(PBASE+0x00201024))
#define UART0_FBRD      ((volatile unsigned int*)(PBASE+0x00201028))
#define UART0_LCRH      ((volatile unsigned int*)(PBASE+0x0020102C))
#define UART0_CR        ((volatile unsigned int*)(PBASE+0x00201030))
#define UART0_IMSC      ((volatile unsigned int*)(PBASE+0x00201038))
#define UART0_ICR       ((volatile unsigned int*)(PBASE+0x00201044))

void uart_init();
void uart_send_byte(char byte);
char uart_receive_byte(void);

extern void uart_send_data(char* ptr, unsigned int length);
extern void uart_recv_data(char* buf, unsigned int len);

extern void uart_send_uint32(unsigned int);
extern void uart_send_uint64(unsigned long);
extern unsigned int uart_receive_uint32();
extern unsigned long uart_receive_uint64();


#endif