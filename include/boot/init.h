#ifndef BOOT_INIT_H
#define BOOT_INIT_H

typedef enum boot_state {
    bootloader_started = 0x0,
    new_kernel_loaded
} boot_state_t;

#define NCORES  4

extern char __start[0];
extern char __end[0];

extern volatile boot_state_t boot_state;

#endif