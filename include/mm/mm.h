#ifndef MM_H
#define MM_H
#include <periph/base.h>

#define LOW_MEMORY  (0x80000)
#define HIGH_MEMORY (PBASE)
#define STACK_SIZE  (0x1000)

#endif