#include <periph/gpio.h>
#include <periph/mbox.h>
#include <periph/uart.h>

void uart_init()
{
    register unsigned int r;

    /* initialize UART */
    *UART0_CR = 0;         // turn off UART0

    /* set up clock for consistent divisor values */
    mbox[0] = 9*4;
    mbox[1] = MBOX_REQUEST;
    mbox[2] = MBOX_TAG_SETCLKRATE; // set clock rate
    mbox[3] = 12;
    mbox[4] = 8;
    mbox[5] = 2;           // UART clock
    mbox[6] = 4000000;     // 4Mhz
    mbox[7] = 0;           // clear turbo
    mbox[8] = MBOX_TAG_LAST;
    mbox_call(MBOX_CH_PROP);

    /* map UART0 to GPIO pins */
    r=*GPFSEL1;
    r&=~((7<<12)|(7<<15)); // gpio14, gpio15
    r|=(4<<12)|(4<<15);    // alt0
    *GPFSEL1 = r;
    *GPPUD = 0;            // enable pins 14 and 15
    r=150; while(r--) { asm volatile("nop"); }
    *GPPUDCLK0 = (1<<14)|(1<<15);
    r=150; while(r--) { asm volatile("nop"); }
    *GPPUDCLK0 = 0;        // flush GPIO setup

    *UART0_ICR = 0x7FF;    // clear interrupts
    *UART0_IBRD = 2;       // 115200 baud
    *UART0_FBRD = 0xB;
    *UART0_LCRH = 0x7<<4;  // 8n1, enable FIFOs
    *UART0_CR = 0x301;     // enable Tx, Rx, UART
}

void uart_send_byte(char c){
    /* wait until we can send */
    do{asm volatile("nop");}while(*UART0_FR&0x20);
    /* write the character to the buffer */
    *UART0_DR=c;
}

char uart_receive_byte(void){
    char r;
    /* wait until something is in the buffer */
    do{asm volatile("nop");}while(*UART0_FR&0x10);
    /* read it and return */
    r=(char)(*UART0_DR);
    return r;
}

inline void uart_send_data(char* ptr, unsigned int length){
    for(unsigned int i = 0 ; i < length; i++){
        uart_send_byte(ptr[i]);
    }
}

inline void uart_recv_data(char* buf, unsigned int len){
    for(unsigned int i = 0 ; i < len; i++){
        buf[i] = uart_receive_byte();
    }
}

inline void uart_send_uint32(unsigned int value) { 
    uart_send_data((char*)&value, sizeof(unsigned int));
}

inline void uart_send_uint64(unsigned long value) { 
    uart_send_data((char*)&value, sizeof(unsigned long));
}

inline unsigned int uart_receive_uint32(){
    unsigned int result;
    uart_recv_data((char*)&result, sizeof(unsigned int));
    return result;
}

inline unsigned long uart_receive_uint64(){
    unsigned long result;
    uart_recv_data((char*)&result, sizeof(unsigned long));
    return result;
}