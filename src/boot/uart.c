#include <boot/init.h>
#include <periph/uart.h>

volatile char * volatile kernel;
volatile boot_state_t boot_state;

void uart_loader(volatile unsigned int core_id){
    if(core_id == 0) {
        uart_init();
        // greeting
        uart_send_byte('R');
        uart_send_byte('P');
        uart_send_byte('3');
        uart_send_byte('b');
        uart_send_byte('o');
        uart_send_byte('o');
        uart_send_byte('t');

        // send __start and __end
        uart_send_uint64((unsigned long)__start);
        uart_send_uint64((unsigned long)__end);

        // receive kernel_address
        kernel = (volatile char * volatile)uart_receive_uint64();
        uart_send_uint64((unsigned long)kernel);

        // receive size
        unsigned int kernel_size = uart_receive_uint32();
        uart_send_uint32(kernel_size);

        // receive kernel
        for(unsigned int i = 0; i < kernel_size; i++){
            kernel[i] = uart_receive_byte();
            uart_send_byte(kernel[i]);
        }

        boot_state = new_kernel_loaded;
        asm volatile ("sev");
    } else {
        while(boot_state != new_kernel_loaded){
            asm volatile ("wfe");
        }
    }

    // restore arguments and jump to the new kernel.
    asm volatile (
        "mov x0, x10;"
        "mov x1, x11;"
        "mov x2, x12;"
        "mov x3, x13;"
        // we must force an absolute address to branch to
        "mov x30, 0x0; ret"
    );
}
