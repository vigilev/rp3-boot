

void memcpy(char* from, char* to, unsigned int size){
    for(unsigned int i = 0; i < size; i++){
        to[i] = from[i];
    }
}